<?php

/*
 *  :)
 */

require('Interfaces/DataSourceInterface.php');
require('Interfaces/CacheInterface.php');
require('Interfaces/DbInterface.php');
require('Interfaces/HttpFetcherInterface.php');
require('Components/Cache.php');
require('Components/Db.php');
require('Components/HttpFetcher.php');
require('Components/DataLoader.php');
require('DataSources/CacheDataSource.php');
require('DataSources/DbDataSource.php');
require('DataSources/HttpDataSource.php');
