<?php

namespace App;

include('autoloader.php');

use App\Components\DataLoader;
use App\Components\Cache;
use App\Components\Db;
use App\Components\HttpFetcher;
use App\DataSources\CacheDataSource;
use App\DataSources\DbDataSource;
use App\DataSources\HttpDataSource;


$config = [
    'url' => 'http://www.datasource.com'
];


// !!! порядок DataSources имеет значение !!!
try {
    $currency_rate = (new DataLoader([
        new CacheDataSource(new Cache()),
        new DbDataSource(new Db()),
        new HttpDataSource(new HttpFetcher($config['url']))
    ]))->getValue('USD');

    print($currency_rate . PHP_EOL);

} catch (\Exception $e) {
    print('Error: ' . $e->getMessage() . PHP_EOL);
}