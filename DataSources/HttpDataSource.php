<?php

namespace App\DataSources;

use App\Interfaces\DataSourceInterface;
use App\Interfaces\HttpFetcherInterface;

class HttpDataSource implements DataSourceInterface
{
    /**
     * @var HttpFetcherInterface
     */
    protected $http_fetcher;

    public  function __construct(HttpFetcherInterface $http_fetcher)
    {
        $this->http_fetcher = $http_fetcher;
    }

    public function getValue(string $key) : ?string
    {
        try {
            $result = $this->http_fetcher->fetchById($key);
        } catch (\Exception $e) {
            //тут можно что-нибудь записать в лог, который нужно инжектировать через конструктор
            return null;
        }

        return $result;
    }

    public function setValue(string $key, string $value) : void
    {
        throw new \Exception('Data source is read only!');
    }

    public function isReadOnly() : bool
    {
        return true;
    }
}