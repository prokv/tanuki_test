<?php

namespace App\DataSources;

use App\Interfaces\DataSourceInterface;
use App\Interfaces\DbInterface;

class DbDataSource implements DataSourceInterface
{
    /**
     * @var DbInterface
     */
    protected $db;

    public  function __construct(DbInterface $db)
    {
        $this->db = $db;
    }

    public function getValue(string $key) : ?string
    {
        return $this->db->findById($key);
    }

    public function setValue(string $key, string $value) : void
    {
        $this->db->insert($key, $value);
    }

    public function isReadOnly() : bool
    {
        return false;
    }
}