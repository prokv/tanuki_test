<?php

namespace App\DataSources;

use App\Interfaces\DataSourceInterface;
use App\Interfaces\CacheInterface;

class CacheDataSource implements DataSourceInterface
{
    /**
     * @var CacheInterface
     */
    protected $cache;

    public  function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function getValue(string $key) : ?string
    {
        return $this->cache->get($key);
    }

    public function setValue(string $key, string $value) : void
    {
        $this->cache->set($key, $value);
    }

    public function isReadOnly() : bool
    {
        return false;
    }
}