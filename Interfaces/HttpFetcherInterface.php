<?php

namespace App\Interfaces;

Interface HttpFetcherInterface
{
    public function fetchById(string $key) : string;
}