<?php

namespace App\Interfaces;

Interface DataSourceInterface
{
    public function getValue(string $key) : ?string;
    public function setValue(string $key, string $value) : void;
    public function isReadOnly() : bool;
}