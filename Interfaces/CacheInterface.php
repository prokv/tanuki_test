<?php

namespace App\Interfaces;

Interface CacheInterface
{
    public function get(string $key) : ?string;
    public function set(string $key, string $value) : void;
    public function exists(string $key) : bool;
    public function destroy(string $key) : void;
}