<?php

namespace App\Interfaces;

Interface DbInterface
{
    public function findById(string $key) : ?string;
    public function insert(string $key, string $value) : void;
}