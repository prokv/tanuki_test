<?php

namespace App\Components;

use App\Interfaces\CacheInterface;

class Cache implements CacheInterface
{
    protected $data = [];

    public function get(string $key) : ?string
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        } else {
            return null;
        }
    }

    public function set(string $key, string $value) : void
    {
        $this->data[$key] = $value;
    }

    public function exists(string $key) : bool
    {
        return !empty($this->data[$key]);
    }

    public function destroy(string $key) : void
    {
        unset($this->data[$key]);
    }
}