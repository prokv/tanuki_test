<?php

namespace App\Components;

use App\Interfaces\HttpFetcherInterface;

class HttpFetcher implements HttpFetcherInterface
{
    //заглушка
    protected $data = [
        'USD' => 65.34,
        'EUR' => 71.39
    ];

    protected $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function fetchById(string $key) : string
    {
        //тут какой-то запрос по $url

        if (!isset($this->data[$key])) {
            throw new \Exception('Cannot load data');
        }

        return $this->data[$key];
    }
}