<?php

namespace App\Components;

use App\Interfaces\DataSourceInterface;

class DataLoader 
{
    protected $data_sources = [];

    public function __construct(array $data_sources)
    {
        foreach ($data_sources as $data_source) {
            $this->addSource($data_source);
        }
    }

    protected function addSource(DataSourceInterface $data_source)
    {
        $this->data_sources[] = $data_source;
    }

    public function getValue(string $key) : string
    {
        $result = null;
        $data_sources_to_set = [];

        /** @var DataSourceInterface $data_source */
        foreach ($this->data_sources as $data_source) {
            $result = $data_source->getValue($key);

            if (!is_null($result)) {
                break;
            }

            if (!$data_source->isReadOnly()) {
                $data_sources_to_set[] = $data_source;
            }
        }

        if (is_null($result) ) {
            throw new \Exception(sprintf('Cannot load data with index %s', $key));
        }

        /** @var DataSourceInterface $data_source_to_set */
        foreach($data_sources_to_set as $data_source_to_set) {
            $data_source_to_set->setValue($key, $result);
        }

        return $result;
    }
}