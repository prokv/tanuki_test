<?php

namespace App\Components;

use App\Interfaces\DbInterface;

class Db implements DbInterface
{
    //заглушка
    protected $data = [];

    public function findById(string $key) : ?string
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        } else {
            return null;
        }
    }

    public function insert(string $key, string $value) : void
    {
        $this->data[$key] = $value;
    }
}